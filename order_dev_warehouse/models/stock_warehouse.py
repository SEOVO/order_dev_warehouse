from odoo import api, fields, models
from odoo.addons.payment.models.payment_acquirer import ValidationError
class StockWarehouse(models.Model):
    _name = "mystock.warehouse"
    #pedidos   = fields.Many2many('sale.order')
    pedidos     = fields.One2many('sale.order','mystock_warehouse')
    productos   = fields.One2many('line.itemsstock','parent')
    name        = fields.Char(compute='get_name')
    state      = fields.Selection([
                        ('draft','Iniciado'),
                        ('progress','En Progreso'),('done','Finalizado')
                    ],default='draft')
    state2      = fields.Selection([
                        ('draft','Iniciado'),
                        ('progress','En Progreso'),('done','Finalizado')
                    ])
    @api.depends('pedidos')
    def get_name(self):
        for record in self:
            name = ""
            for p in record.pedidos:
                name += p.name+' /'
            record.name = name
            record.state2 = record.state

    def update_items(self):
        #self.productos.unlink()
        for record in self:
            record.state = 'progress'
            record.productos.unlink()
            for pedidos in record.pedidos:
                for line in pedidos.order_line:
                    producto = line.product_id
                    #buscar el stock
                    stocks = self.env['stock.quant'].search([('product_id','=',producto.id),
                                                              ('location_id.location_id','!=',False),
                                                             ('location_id.usage','in',['internal'])])
                    #raise ValidationError(stocks)
                    linex = self.env['line.itemsstock'].search([
                        ('producto','=',line.product_id.id),
                        ('parent','=',record.id)
                    ])
                    if linex:
                        linex.cantidad = linex.cantidad + line.product_uom_qty

                    else:
                        linex = self.env['line.itemsstock'].create({
                              'producto': line.product_id.id,
                              'parent': record.id,
                              'cantidad':line.product_uom_qty
                        })
                    json_ubi =   []
                    #json_ubi2 = dict()
                    json_ubi2 =   []
                    linex.ubicaciones = False
                    linex.congelar.unlink()
                    for stk in stocks:
                        #linex += stk.location_id
                        json_ubi.append(stk.location_id.id)
                        dd = {
                            #'ubicacion':stk.location_id.id,
                            'stock_quant':stk.id,
                            #'pickind_type':

                        }

                        almacen = self.env['stock.warehouse'].search([('company_id','=',stk.location_id.company_id.id)])

                        if almacen:
                            almacen = almacen[0]
                            picking = self.env['stock.picking.type'].search([('warehouse_id','=',almacen.id),
                                                              ('code','=','outgoing'),
                                                             ])
                            picking = picking[0]
                            if picking:
                                dd['pickind_type'] = picking.id
                        linex.write({
                            #'ubicaciones' : [(6 ,0 ,json_ubi)],
                            'congelar': [(0 ,0 ,dd)]
                        })

                    #raise ValidationError(json_ubi)
                    linex.write({
                        'ubicaciones' : [(6 ,0 ,json_ubi)],
                        #'congelar': [(0 ,0 ,json_ubi)]
                    })

    def congelar_items(self):
        for record in self:
            contador = 0
            record.state = 'done'
            items = record.productos
            for i in items:
                line = i.congelar
                for l in line:
                    cantidad = l.cantidad_separar
                    if cantidad > 0:
                        #crear la salida
                        #customer = self.env['res.partner'].search([('id','=',1)])
                        customer = l.stock_quant.location_id.company_id.partner_id


                        salida = {
                            'partner_id': customer.id,
                            'picking_type_id': l.pickind_type.id,
                            'origin': record.name,
                            'location_id':l.stock_quant.location_id.id,
                            'location_dest_id': customer.property_stock_customer.id,
                            'company_id':l.stock_quant.location_id.company_id.id

                        }
                        if l.transferencia:
                            l.transferencia.unlink()
                        trf = self.env['stock.picking'].create(salida)

                        l.write({'transferencia' : trf.id})

                        trf.move_ids_without_package += self.env['stock.move'].new({
                            'product_id':i.producto.id,
                            'product_uom_qty': l.cantidad_separar,
                            'name': i.producto.name,
                            'product_uom': i.producto.uom_id.id,
                            'location_id':l.stock_quant.location_id.id,
                            'location_dest_id':  customer.property_stock_customer.id,
                            'quantity_done' : l.cantidad_separar,
                            'company_id':l.stock_quant.location_id.company_id.id
                        })
                        
                        trf.action_confirm()
                        trf.action_assign()

                        #trf.write({'qty_done' : l.cantidad_separar})
                        #trf.qty_done = l.cantidad_separar
                        trf.button_validate()
                        contador +=1
            if contador == 0:
                raise ValidationError('No ingreso ninguna cantidad')

            for p in record.pedidos:
                if p.state in ['draft','sent']:
                    p.action_confirm()

    def unlink(self):
        if self.state in ['progress','done']:
            raise ValidationError('Solo se puede borrar el estado Iniciado')
        return super(StockWarehouse, self).unlink()
    def cancelar_state(self):
        self.state = 'draft'
        '''
        for p in self.pedidos:
                #if p.state in ['draft','sent']:
                p.action_cancel()
        '''



class LineItems(models.Model):
    _name       = "line.itemsstock"
    parent      = fields.Many2one('mystock.warehouse')
    producto    = fields.Many2one('product.product')
    cantidad    = fields.Float()
    ubicaciones = fields.Many2many('stock.location')
    congelar    = fields.One2many('separar.productos','line_items',string="Ubicaciones")
    cantidad_ingresada = fields.Float(compute='get_cantidades')
    limit = fields.Float(compute='get_cantidades')
    _sql_constraints = [
        ('producto','parent', 'There can be no duplication Items')
    ]
    def get_cantidades(self):
        for record in self:
            total = 0
            for ubi in record.congelar:
                total += ubi.cantidad_separar
            record.cantidad_ingresada = total
            record.limit = record.cantidad - total

    def funcion_confi(self):
        if self.parent.state == 'done':
            raise ValidationError('Ya no se puede configurar')
        return {
                    'name'     : ('Configurar Cantidades'),
                    'type'     : 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'configurar.cantidades',
                    'target'   : 'new',
                    #'res_id': wiz.id,
                    #'context'  : {'default_product': self.id}
                    'context'  : {'default_line_items': self.id}
                }


class SepararProductos(models.Model):
    _name            = "separar.productos"
    name        = fields.Char(compute='get_name')
    pickind_type     = fields.Many2one('stock.picking.type' ,
                                       domain=[('code','=','outgoing')])
    stock_quant      = fields.Many2one('stock.quant')
    ubicacion        = fields.Many2one('stock.location',related='stock_quant.location_id')
    cantidad_separar = fields.Float()
    line_items       = fields.Many2one('line.itemsstock')
    cantidad_maxima  = fields.Float(related='stock_quant.quantity')
    transferencia    = fields.Many2one('stock.picking')

    def get_name(self):
        for record in self:
            name = str(record.ubicacion.complete_name)
            if record.cantidad_separar:
                name += " ("+str(record.cantidad_separar)+")"

            record.name = name






