from odoo import api, fields, models

class Orders(models.Model):
    _inherit            = 'sale.order'
    mystock_warehouse   = fields.Many2one('mystock.warehouse')
