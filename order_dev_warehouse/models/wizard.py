from odoo import api, fields, models
from odoo.addons.payment.models.payment_acquirer import ValidationError
class WooUnitExport(models.TransientModel):
    _name             = "configurar.cantidades"
    line_items        = fields.Many2one('line.itemsstock')
    stock             = fields.Many2one('separar.productos')
    cantidad          = fields.Float()
    cantidad_maxima   = fields.Float(related="stock.cantidad_maxima",string="Cantidad en Stock")
    maximo            = fields.Float(related="line_items.limit")
    max               = fields.Float(compute='get_max',string="Maximo a Ingresar")
    @api.depends('stock')
    def get_max(self):
        self.max = self.maximo + self.stock.cantidad_separar
    @api.onchange('stock')
    def change_cantidad(self):
        for record in self:
            record.cantidad = record.stock.cantidad_separar
    def update_stock(self):
        stock = self.stock
        if self.cantidad > self.max:
            raise ValidationError('Ingrese una cantidad menor')
        if self.cantidad > self.cantidad_maxima:
            raise ValidationError('Ingrese una cantidad menor')
        stock.write({
            'cantidad_separar': self.cantidad
        })

