# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.addons.payment.models.payment_acquirer import ValidationError

class UpdatePriceCategorias(models.TransientModel):
    _name = 'update.price_categorias'
    _description  = "Actualizar Precio de las categorias"
    categorias    = fields.Many2many('product.category',required=True)
    variacion     = fields.Float(required=True)
    def update_price(self):
        for record in self:
            for cat in record.categorias:
                productos = self.env['product.product'].search([('categ_id','=',cat.id)])
                for p in productos:
                    p.list_price = record.variacion + p.list_price


class UpdatePricetipocambio(models.TransientModel):
    _name = 'update.price_cambio'
    _description  = "Actualizar Precio por Tipo de Cambio"
    moneda        = fields.Many2one('product.pricelist',string="Lista de Precio",required=True)
    variacion     = fields.Float(required=True,String="Unidad Tipo de Cambio")
    def update_price(self):
        for record in self:
            productos = self.env['product.product'].search([('sale_ok','=',True)])
            for p in productos:
                valor_actual = p.list_price
                #buscar la lista de precio de los productos
                lista = self.env['product.pricelist.item'].search([('pricelist_id','=',record.moneda.id),
                                                                   ('product_id','=',p.id)])
                if not lista:
                    lista = self.env['product.pricelist.item'].create({
                               'pricelist_id' : record.moneda.id,
                               'product_id' : p.id,
                               #'product_tmpl_id':p.	product_tmpl_id.id,
                               'fixed_price': p.lst_price * record.variacion,
                               'applied_on': '0_product_variant'

                     })
                else:
                    lista.fixed_price = p.lst_price * record.variacion
                    lista.applied_on = '0_product_variant'





