# -*- coding: utf-8 -*-
{
    'name': "order_dev_warehouse",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','sale_management'],

    # always loaded
    'data': [
         'security/ir.model.access.csv',

        'views/templates.xml',
        'views/tipo_cambio.xml',
        'views/views.xml',
        'views/stock_warehouse.xml',
        'views/line_items.xml',
        'views/wizard.xml'
        #'views/maps.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
